const nombre = document.getElementById("elnombre");
const apellido = document.getElementById("elapellido");
const correo = document.getElementById("elcorreo");
const celular = document.getElementById("elcelular");
const cedula=document.getElementById("lacedula")
const ciudad=document.getElementById("laciudad")
const direccion=document.getElementById("ladireccion")
const terminoycondiciones = document.getElementById("Terminoycondiciones");
const form = document.getElementById("form");
const listInputs = document.querySelectorAll("#caracteristicas"); 
   
form.addEventListener("submit", (e) => {
    e.preventDefault();
    let condicion = validacionForm();
    if (condicion) {
      enviarFormulario();
    }
  });
  
  function validacionForm() {
    form.lastElementChild.innerHTML = "";
    let condicion = true;
    listInputs.forEach((element) => {
      element.lastElementChild.innerHTML = "";
    });
    if (cedula.value.length != 10 || cedula.value.trim() == "") {
      mostrarMensajeError("lacedula", "Cedula no valida*");
      condicion = false;
    }
    if (direccion.value.length < 1 || direccion.value.trim() == "") {
        mostrarMensajeError("ladireccion", "direccion no valida*");
        condicion = false;
      }
    if (ciudad.value.length <1  || ciudad.value.trim() == "") {
        mostrarMensajeError("laciudad", "Ciudad no valida*");
        condicion = false;
      }
  
    if (nombre.value.length < 1 || nombre.value.trim() == "") {
      mostrarMensajeError("elnombre", "Nombre no valido*");
      condicion = false;
    }
    if (apellido.value.length < 1 || apellido.value.trim() == "") {
      mostrarMensajeError("elapellido", "Apellido no valido");
      condicion = false;
    }
    if (correo.value.length < 1 || correo.value.trim() == "") {
      mostrarMensajeError("elcorreo", "Correo no valido*");
      condicion = false;
    }
    if (celular.value.length != 10 ||celular.value.trim() == "" ||isNaN(celular.value)){
      mostrarMensajeError("elcelular", "Celular no valido*");
      condicion = false;
    }
    
    if (!terminoycondiciones.checked) {
      mostrarMensajeError("Terminoycondiciones", "Acepte*");
      condicion = false;
    } else {
      mostrarMensajeError("Terminoycondiciones", "");
    }
    return condicion;
  }
  
  function mostrarMensajeError(claseInput, mensaje) {
    let elemento = document.querySelector(`.${claseInput}`);
    elemento.lastElementChild.innerHTML = mensaje;
  }
  
  function enviarFormulario() {
    form.reset();
    form.lastElementChild.innerHTML = "Listo !!";
  }
  
  
